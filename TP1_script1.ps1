﻿<#David Roussat 22/10/20 Script info syst#>

<#Nom de la machine#>
Write-Host "Name machine : " -NoNewline; $env:computername
""

<#Ip principal#>
$Ip = (Test-Connection -ComputerName $env:computername -count 1).ipv4address.IPAddressToString
Write-Host  "Ip principal : " -NoNewline ; $Ip

""

<#OS#>
$Os = (Get-WmiObject -class Win32_OperatingSystem).Caption 
Write-Host  "OS : " -NoNewline ; $Os
""

<#OS version#>
$Version = (Get-CimInstance Win32_OperatingSystem).version
Write-Host "version : " -NoNewline ; $Version
""

<#Root Date#>
$date = net stats workstation | findstr "depuis"
$CharArray = $date.Split(" ")
Write-Host "Date et heure d'allumage:" ; $CharArray[2, 3]  
""

<#Update system#>
$Update = (New-Object -com "Microsoft.Update.AutoUpdate"). Results | Select-Object -ExpandProperty LastInstallationSuccessDate 
$Date = (Get-Date).AddDays(-30)

if ($Update -lt $Date) {
    $up = $false
}
else {
    $up = $true
} 

Write-Host "Is Update : " -NoNewline; $up
""
Write-Host "Since : " -NoNewline ; $Update
""

<#RAM#>
$RamTotale = gwmi Win32_OperatingSystem | % {
    $_.TotalVisibleMemorySize
}

$RamDisponible = gwmi Win32_OperatingSystem | % {
    $_.FreePhysicalMemory
}
<#RAM free#>
Write-Host "RAM"
$s = $RamDisponible / 1000000
Write-Host "    Ram free = " -NoNewline; Write-Host "$s GB"
<#RAM used#>
$RamUtilise = ($RamTotale - $RamDisponible) / 1000000
Write-Host "    Ram used = " -NoNewline; Write-Host "$RamUtilise GB"
""
<#Disk used#>
Write-Host "Disk"
$disk = get-psdrive C
$used_size = ($disk.used / 1GB)
write-host "    Disk used : " -NoNewline; Write-Host " $used_size GB"

<#Disk free#>
$disk = get-psdrive C
$free_size = ($disk.free / 1GB)
write-host "    Disk free: " -NoNewline ; Write-Host "$free_size GB"
""
<#Users#>
$user = Get-LocalUser
$name = $user.name
Write-Host "Users list: " $name
""

<#PING#>
$CompName = "8.8.8.8"
foreach ($comp in $CompName) {
    $test = (Test-Connection -ComputerName $comp -Count 4  | measure-Object -Property ResponseTime -Average).average
    $response = ($test -as [int] )
    write-Host "8.8.8.8 average ping time" -NoNewline; write-Host " is " -NoNewline; ; Write-Host "$response ms" 
}
