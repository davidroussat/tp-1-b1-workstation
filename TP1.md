# TP 1 B1 Workstation

# I. Self-footprinting

## 1. Host OS

###  Nom de la machine:
*PS C:\Users\rouss> systeminfo*

	Nom de l’hôte: LAPTOP-9S2RQ9JE
	[...]

###  OS et version: 
*PS C:\Users\rouss>wmic os get CSDVersion,Manufacturer,Name,OperatingSystemSKU,OSArchitecture,Version /value*
	
	[...]
	Nom du système d’exploitation:              Microsoft Windows 10 Famille
	Version du système:                         10.0.18363 N/A version 18363
	[...]

### Architecture processeur (32-bit, 64-bit, ARM, etc):
*PS C:\Users\rouss>wmic os get               CSDVersion,Manufacturer,Name,OperatingSystemSKU,OSArchitecture,Version /value*

	[...]
	OSArchitecture=64 bits
	[...]


### Quantité RAM et modèle de la RAM:
*PS C:\Users\rouss> systeminfo*

	[...]
	Mémoire physique totale: 16 156 Mo
	Mémoire physique disponible:8 672 Mo
	[...]

### Modèle de la RAM:
*PS C:\Users\rouss>wmic memorychip get devicelocator, manufacturer*

    DeviceLocator   Manufacturer
    ChannelA-DIMM0  SK Hynix
    ChannelB-DIMM0  SK Hynix
 
## 2. Devices

### Marque et  modèle du processeur:
*PS C:\Users\rouss>Get-WmiObject win32_processor*

	Caption           : Intel64 Family 6 Model 126 Stepping 5
	[...]
	Name              : Intel(R) Core(TM) i7-1065G7 CPU @ 1.30GHz
	[...]

### Identifier le nombre de processeurs:
*PS C:\Users\rouss> systeminfo*

	[...]
	Processeur(s): 1 processeur(s) installé(s).
	[...]
    
### Nombre de coeur:
*PS C:\Users\rouss> wmic cpu get NumberOfCores*

    NumberOfCores
    4

### Nom du processeur:

    Intel(R) Core(TM)  = nom du modèle 
	i7 = niveau de performance
	10 = génération de processeur
	065 = sert à différencier les caractéristiques au sein d'une famille de processeurs.
	G7 CPU = désigne un CPU avec des graphiques intégrés supplémentaire
	1.30GHz: C'est la fréquence correspondant à la vitesse de calculde l'ordinateur.

### Marque et Modèlede du touchpad:

?????????????????????????????????????????

### Marque et Modèlede la carte graphique:
*PS C:\Users\rouss>wmic path Win32_VideoController get Name /value*

    Name=Intel(R) Iris(R) Plus Graphics

### Disque dur

### Identifier la marque et le modèle de votre(vos) disque(s) dur(s):
*PS C:\Users\rouss> wmic diskdrive get model*

	Modele 	
	WDC PC SN520 SDAPNUW-512G-1014

### Identifier les différentes partitions de votre/vos disque(s) dur(s):
*PS C:\Users\rouss> diskpart*
*DISKPART> list partition*

	  N° partition   Type              Taille   Décalage
 	 -------------  ----------------  -------  --------
 	 Partition 1    Système            100 M   1024 K
 	 Partition 2    Réservé             16 M    101 M
  	 Partition 3    Principale         475 G    117 M
  	 Partition 4    Récupération      1024 M    475 G

### Déterminer le système de fichier de chaque partition :
*DISKPART> select partition 1*
*DISKPART> detail partition*

	Partition 1
	Type    : c12a7328-f81f-11d2-ba4b-00a0c93ec93b
	Masqué  : Oui
	Requis  : Non
	Attrib  : 0X8000000000000000
	Décalage en octets : 1048576

 	 N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
 	 ----------  ---  -----------  -----  ----------  -------  ---------  --------
	 Volume 1         ESP          FAT32  Partition    100 M   Sain       Système



				Fs
	Système fichier:	----- 
				FAT32 

*DISKPART> select partition 2*
*DISKPART> detail partition*

	Partition 2
	Type    : e3c9e316-0b5c-4db8-817d-f92df00215ae
	Masqué  : Oui
	Requis  : Non
	Attrib  : 0X8000000000000000
	Décalage en octets : 105906176

	Il n’y a pas de volume associé avec cette partition.

*DISKPART> select partition 3*
*DISKPART> detail partition*
	
				Fs
	Système fichier: 	-----
				NTFS  

*DISKPART> select partition 4*
*DISKPART> detail partition*

				Fs
	Système fichier: 	-----
				NTFS  


### Expliquer la fonction de chaque partition:
*PS C:\Users\rouss> diskpart*
*DISKPART> list partition*

	  N° partition   Type              Taille   Décalage
 	 -------------  ----------------  -------  --------
 	 Partition 1    Système            100 M   1024 K
 	 Partition 2    Réservé             16 M    101 M
  	 Partition 3    Principale         475 G    117 M
  	 Partition 4    Récupération      1024 M    475 G

Partition 1 : Système
	La partition système EFI (ESP) est une petite partition formatée en FAT32. Il y existe des fichiers exécutables dont l'extension est .efi pour le système installé et les applications utilisées par le microprogramme au démarrage. 


Partition 2 : Réservé
	Bitlocker a besoin de séparer les fichiers de boot et les fichiers windows, elle est principalement utilisée comme partition de chiffrement et d’encryption.
	Elle contient aussi l’Environnement de récupération WinRE  qui va permettre de récupérer le système en cas de problème de démarrage, par la ligne Réparer l’ordinateur dans les options avancées de démarrage (appelées par F8 au démarrage).

Partition 3 : Principale
	Contient un système de fichier correspondant au système d'exploitation installé sur celle-ci.


Partition 4 : Récupération
	La partition de récupération est une petite partition sur votre disque dur, elle est destinée à vous aider à restaurer votre Windows ou à résoudre les problèmes du système. Il existe deux types de partitions de récupération que vous pouvez voir dans Windows 10, 8 et 7.

## 3. Users

### La liste complète des utilisateurs de la machine (je vous vois les Windowsiens...):
*PS C:\Users\rouss> WMIC USERACCOUNT LIST BRIEF*

	AccountType  Caption                             Domain           FullName       Name                SID
	512          LAPTOP-9S2RQ9JE\Administrateur      LAPTOP-9S2RQ9JE                 Administrateur      S-1-5-21-2472448776-1101701207-1133128792-500
	512          LAPTOP-9S2RQ9JE\DefaultAccount      LAPTOP-9S2RQ9JE                 DefaultAccount      S-1-5-21-2472448776-1101701207-1133128792-503
	512          LAPTOP-9S2RQ9JE\Invité              LAPTOP-9S2RQ9JE                 Invité              S-1-5-21-2472448776-1101701207-1133128792-501
	512          LAPTOP-9S2RQ9JE\rouss               LAPTOP-9S2RQ9JE  david roussat  rouss               S-1-5-21-2472448776-1101701207-1133128792-1001
	512          LAPTOP-9S2RQ9JE\WDAGUtilityAccount  LAPTOP-9S2RQ9JE                 WDAGUtilityAccount  S-1-5-21-2472448776-1101701207-1133128792-504



### Déterminer le nom de l'utilisateur qui est full admin sur la machine:
	??? 


NT-AUTHORITY\SYSTEM:
    NT-AUTHORITY \ SYSTEM est le nom d'un ID de sécurité, qui n'est ni un groupe ni un compte. Il est affiché dans le Gestionnaire des tâches en tant que SYSTÈME s'il s'agit du SID principal d'un programme. J'appellerais tout au plus "un pseudo compte".

Concept de SID:
    Dans Windows, les SID (Security Identifier) sont des identifiants uniques alphanumériques qui identifient chaque système, utilisateur ou objet (groupe) dans un réseau ou sur un PC.

## 4. Processus

### Déterminer la liste des processus de la machine
*PS C:\Users\rouss> tasklist*	


    Nom de l’image                 PID Nom de la sessio Numéro de s Utilisation
    ========================= ======== ================ =========== ============
    System Idle Process              0 Services                   0         8 Ko
    System                           4 Services                   0        20 Ko
    Registry                       120 Services                   0    41 064 Ko
    smss.exe                       548 Services                   0       480 Ko
    csrss.exe                      652 Services                   0     2 524 Ko
    wininit.exe                    740 Services                   0     2 496 Ko
    services.exe                   812 Services                   0     8 064 Ko
    lsass.exe                      832 Services                   0    19 468 Ko
    svchost.exe                     88 Services                   0     1 172 Ko
    fontdrvhost.exe                608 Services                   0     1 312 Ko
    svchost.exe                    556 Services                   0    50 000 Ko
    WUDFHost.exe                   972 Services                   0    14 324 Ko
    svchost.exe                   1084 Services                   0    18 076 Ko
    svchost.exe                   1132 Services                   0     5 316 Ko
    WUDFHost.exe                  1304 Services                   0     2 084 Ko
    svchost.exe                   1400 Services                   0     2 472 Ko
    svchost.exe                   1420 Services                   0     8 600 Ko
    svchost.exe                   1436 Services                   0     2 412 Ko
    svchost.exe                   1448 Services                   0     3 336 Ko
    svchost.exe                   1472 Services                   0     3 972 Ko
    svchost.exe                   1612 Services                   0     5 488 Ko
    svchost.exe                   1704 Services                   0    13 048 Ko
    svchost.exe                   1756 Services                   0     5 980 Ko
    svchost.exe                   1784 Services                   0     6 648 Ko
    svchost.exe                   1796 Services                   0     5 792 Ko
    svchost.exe                   1832 Services                   0     2 692 Ko
    svchost.exe                   1988 Services                   0     5 240 Ko
    svchost.exe                   1104 Services                   0     2 320 Ko
    svchost.exe                   1336 Services                   0     6 484 Ko
    svchost.exe                   1572 Services                   0     4 160 Ko
    svchost.exe                   1900 Services                   0     4 204 Ko
    svchost.exe                   2164 Services                   0     3 860 Ko
    svchost.exe                   2236 Services                   0     5 360 Ko
    svchost.exe                   2340 Services                   0     3 892 Ko
    igfxCUIServiceN.exe           2420 Services                   0     6 080 Ko
    svchost.exe                   2432 Services                   0    17 324 Ko
    svchost.exe                   2440 Services                   0     6 608 Ko
    svchost.exe                   2504 Services                   0    14 088 Ko
    svchost.exe                   2620 Services                   0     8 564 Ko
    svchost.exe                   2632 Services                   0     1 652 Ko
    Memory Compression            2732 Services                   0   408 984 Ko
    svchost.exe                   2768 Services                   0     5 544 Ko
    svchost.exe                   2776 Services                   0     4 056 Ko
    svchost.exe                   2784 Services                   0     5 228 Ko
    svchost.exe                   2792 Services                   0    13 588 Ko
    dasHost.exe                   2924 Services                   0     8 460 Ko
    WUDFHost.exe                  2976 Services                   0     2 744 Ko
    svchost.exe                   2988 Services                   0     9 608 Ko
    svchost.exe                   2556 Services                   0     7 228 Ko
    svchost.exe                   3116 Services                   0     4 304 Ko
    WUDFHost.exe                  3132 Services                   0     2 108 Ko
    svchost.exe                   3252 Services                   0    10 304 Ko
    svchost.exe                   3344 Services                   0     2 736 Ko
    svchost.exe                   3388 Services                   0    14 772 Ko
    svchost.exe                   3488 Services                   0     7 520 Ko
    spoolsv.exe                   3616 Services                   0    12 268 Ko
    svchost.exe                   3676 Services                   0     6 336 Ko
    wlanext.exe                   3744 Services                   0     2 108 Ko
    svchost.exe                   3752 Services                   0    13 192 Ko
    conhost.exe                   3784 Services                   0     2 172 Ko
    svchost.exe                   3816 Services                   0     2 600 Ko
    ACCSvc.exe                    4080 Services                   0     2 552 Ko
    IntelCpHDCPSvc.exe            4092 Services                   0     1 848 Ko
    svchost.exe                      8 Services                   0    46 640 Ko
    CxUIUSvc64.exe                3444 Services                   0     2 944 Ko
    svchost.exe                   3428 Services                   0    37 472 Ko
    GTFidoService.exe             3416 Services                   0     9 944 Ko
    OneApp.IGCC.WinService.ex     3288 Services                   0    10 008 Ko
    CxAudioSvc.exe                3280 Services                   0    15 980 Ko
    Bridge_Service.exe            3268 Services                   0     3 472 Ko
    esif_uf.exe                   3800 Services                   0     2 008 Ko
    svchost.exe                   3804 Services                   0    15 172 Ko
    CxUtilSvc.exe                 4016 Services                   0     3 588 Ko
    svchost.exe                   4132 Services                   0     3 120 Ko
    IntelAudioService.exe         4208 Services                   0    10 916 Ko
    svchost.exe                   4216 Services                   0    14 356 Ko
    NortonSecurity.exe            4364 Services                   0    53 060 Ko
    nsWscSvc.exe                  4372 Services                   0     3 728 Ko
    svchost.exe                   4408 Services                   0     5 868 Ko
    RstMwService.exe              4424 Services                   0     1 656 Ko
    svchost.exe                   4432 Services                   0     1 364 Ko
    svchost.exe                   4444 Services                   0     5 172 Ko
    ThunderboltService.exe        4452 Services                   0     2 788 Ko
    svchost.exe                   4600 Services                   0     7 484 Ko
    svchost.exe                   4648 Services                   0    15 736 Ko
    svchost.exe                   4656 Services                   0     2 280 Ko
    svchost.exe                   4836 Services                   0     1 536 Ko
    svchost.exe                   4884 Services                   0     8 796 Ko
    jhi_service.exe               5352 Services                   0     1 232 Ko
    PresentationFontCache.exe     5852 Services                   0     2 448 Ko
    svchost.exe                   5864 Services                   0     6 404 Ko
    svchost.exe                   6308 Services                   0    15 520 Ko
    svchost.exe                   6752 Services                   0    13 336 Ko
    svchost.exe                   7576 Services                   0     5 412 Ko
    svchost.exe                   8280 Services                   0    11 380 Ko
    dllhost.exe                  10196 Services                   0     5 504 Ko
    SecurityHealthService.exe    10396 Services                   0    13 692 Ko
    QASvc.exe                    13924 Services                   0     4 472 Ko
    WmiPrvSE.exe                 14024 Services                   0     7 800 Ko
    svchost.exe                   4560 Services                   0    13 468 Ko
    svchost.exe                   5772 Services                   0     3 628 Ko
    svchost.exe                   7476 Services                   0    12 012 Ko
    svchost.exe                  11872 Services                   0     6 720 Ko
    SocketHeciServer.exe          1148 Services                   0     1 820 Ko
    SgrmBroker.exe               10084 Services                   0     6 092 Ko
    svchost.exe                  14508 Services                   0     6 144 Ko
    svchost.exe                   5780 Services                   0    14 316 Ko
    svchost.exe                  12412 Services                   0     3 108 Ko
    svchost.exe                  14360 Services                   0     2 256 Ko
    svchost.exe                   1200 Services                   0    44 192 Ko
    svchost.exe                  19356 Services                   0     3 224 Ko
    aesm_service.exe               768 Services                   0     3 308 Ko
    svchost.exe                  17844 Services                   0     3 916 Ko
    csrss.exe                    24544                            9     1 088 Ko
    svchost.exe                   6356 Services                   0     3 932 Ko
    svchost.exe                  19272 Services                   0     2 992 Ko
    svchost.exe                   5384 Services                   0     9 076 Ko
    csrss.exe                    28068                           13     1 096 Ko
    csrss.exe                    43684                           16     1 148 Ko
    svchost.exe                  46672 Services                   0     7 648 Ko
    csrss.exe                    54736                           23     3 072 Ko
    MsMpEng.exe                  57024 Services                   0   609 364 Ko
    NisSrv.exe                   52004 Services                   0    11 512 Ko
    svchost.exe                  62144 Services                   0     7 092 Ko
    vds.exe                      65888 Services                   0     8 840 Ko
    csrss.exe                    67224 Console                   30     4 632 Ko
    winlogon.exe                 34632 Console                   30     8 520 Ko
    fontdrvhost.exe              48312 Console                   30     7 232 Ko
    dwm.exe                      50328 Console                   30    89 736 Ko
    sihost.exe                   32388 Console                   30    24 908 Ko
    svchost.exe                  62220 Console                   30    24 928 Ko
    igfxEMN.exe                  27884 Console                   30    21 688 Ko
    svchost.exe                  65080 Console                   30    27 452 Ko
    taskhostw.exe                62068 Console                   30    16 964 Ko
    svchost.exe                  53696 Services                   0     8 844 Ko
    explorer.exe                 32892 Console                   30   140 536 Ko
    svchost.exe                  56852 Console                   30    27 660 Ko
    dllhost.exe                  60308 Console                   30     5 164 Ko
    ctfmon.exe                   59664 Console                   30    13 892 Ko
    StartMenuExperienceHost.e    25364 Console                   30    93 788 Ko
    RuntimeBroker.exe             7348 Console                   30    23 096 Ko
    SearchUI.exe                 53816 Console                   30   259 644 Ko
    RuntimeBroker.exe            47876 Console                   30    41 956 Ko
    YourPhone.exe                60384 Console                   30    14 168 Ko
    SettingSyncHost.exe          60044 Console                   30     7 016 Ko
    TabTip.exe                   42704 Console                   30    12 532 Ko
    LockApp.exe                  51076 Console                   30    57 012 Ko
    RuntimeBroker.exe            53808 Console                   30    20 864 Ko
    RuntimeBroker.exe            52416 Console                   30    31 072 Ko
    RuntimeBroker.exe            11552 Console                   30    11 220 Ko
    SecurityHealthSystray.exe    34748 Console                   30     6 560 Ko
    OneDrive.exe                 58628 Console                   30    78 976 Ko
    Teams.exe                    64608 Console                   30   109 808 Ko
    Discord.exe                  60528 Console                   30    68 204 Ko
    Teams.exe                    55504 Console                   30    68 032 Ko
    Discord.exe                  65776 Console                   30    99 948 Ko
    Discord.exe                  47884 Console                   30    23 640 Ko
    Teams.exe                    49028 Console                   30    35 440 Ko
    Teams.exe                    14188 Console                   30   344 452 Ko
    IGCCTray.exe                 50004 Console                   30    50 992 Ko
    IGCC.exe                     18556 Console                   30    38 552 Ko
    Skype.exe                    56556 Console                   30    53 744 Ko
    Discord.exe                  66392 Console                   30    10 388 Ko
    Skype.exe                    53952 Console                   30    11 684 Ko
    Discord.exe                  66988 Console                   30   163 732 Ko
    Skype.exe                    54928 Console                   30    33 012 Ko
    Skype.exe                    67300 Console                   30    20 476 Ko
    Skype.exe                    13132 Console                   30    46 216 Ko
    Discord.exe                  40288 Console                   30    15 304 Ko
    Teams.exe                    67340 Console                   30    88 324 Ko
    Teams.exe                    66600 Console                   30   119 272 Ko
    NortonSecurity.exe           57404 Console                   30    15 816 Ko
    QAAgent.exe                  51208 Console                   30     7 324 Ko
    QAAdminAgent.exe             11768 Console                   30    27 196 Ko
    igfxextN.exe                 65988 Console                   30     7 640 Ko
    unsecapp.exe                 57108 Console                   30     7 300 Ko
    QALockHandler.exe            67332 Console                   30     8 284 Ko
    unsecapp.exe                 11856 Console                   30     7 176 Ko
    RuntimeBroker.exe            67056 Console                   30    20 808 Ko
    svchost.exe                  67304 Console                   30    27 904 Ko
    notepad.exe                  60452 Console                   30    14 244 Ko
    ApplicationFrameHost.exe     53084 Console                   30    32 564 Ko
    WinStore.App.exe             53636 Console                   30     2 632 Ko
    RuntimeBroker.exe            49476 Console                   30    19 364 Ko
    SmartAudio3.exe              53632 Console                   30    44 972 Ko
    Flow.exe                     22280 Console                   30    39 420 Ko
    commsapps.exe                65544 Console                   30       924 Ko
    HxTsr.exe                    64712 Console                   30    38 948 Ko
    SystemSettings.exe           56776 Console                   30       848 Ko
    UserOOBEBroker.exe           43184 Console                   30     5 820 Ko
    dllhost.exe                  68576 Console                   30    11 432 Ko
    HostAppServiceUpdater.exe    31600 Console                   30     1 528 Ko
    WindowsInternal.Composabl    61488 Console                   30    42 420 Ko
    ePowerButton_NB.exe          37892 Console                   30     6 264 Ko
    ACCStd.exe                   67560 Console                   30     6 740 Ko
    svchost.exe                  65028 Services                   0    15 408 Ko
    msedge.exe                   55184 Console                   30   177 776 Ko
    msedge.exe                   56744 Console                   30     4 844 Ko
    msedge.exe                   32792 Console                   30   237 688 Ko
    msedge.exe                   67844 Console                   30    57 116 Ko
    msedge.exe                   31580 Console                   30   103 680 Ko
    FileCoAuth.exe               55260 Console                   30    20 372 Ko
    OfficeClickToRun.exe         67984 Services                   0    41 968 Ko
    AppVShNotify.exe             64844 Services                   0     4 580 Ko
    AppVShNotify.exe             46940 Console                   30     5 084 Ko
    SearchIndexer.exe            44368 Services                   0    43 808 Ko
    svchost.exe                  47228 Services                   0     4 132 Ko
    ShellExperienceHost.exe      41416 Console                   30    65 612 Ko
    RuntimeBroker.exe            61400 Console                   30    21 284 Ko
    smartscreen.exe              62328 Console                   30    27 836 Ko
    msedge.exe                   48316 Console                   30    17 464 Ko
    msedge.exe                   53060 Console                   30    94 776 Ko
    msedge.exe                   50628 Console                   30   101 652 Ko
    Microsoft.Photos.exe         55716 Console                   30     2 188 Ko
    RuntimeBroker.exe            56044 Console                   30    13 888 Ko
    svchost.exe                  64052 Services                   0     5 520 Ko
    powershell.exe               58148 Console                   30   132 664 Ko
    conhost.exe                  70460 Console                   30    17 152 Ko
    audiodg.exe                  58240 Services                   0    15 092 Ko
    LiveUpdateChecker.exe        69776 Console                   30     4 264 Ko
    svchost.exe                  70140 Services                   0    20 048 Ko
    msedge.exe                   69688 Console                   30    23 412 Ko
    powershell.exe               62008 Console                   30    83 440 Ko
    conhost.exe                  48172 Console                   30    17 708 Ko
    svchost.exe                  62780 Services                   0     8 540 Ko
    svchost.exe                  62912 Services                   0    12 308 Ko
    SearchProtocolHost.exe       68412 Console                   30     7 804 Ko
    SearchFilterHost.exe         56928 Services                   0     6 224 Ko
    Teams.exe                     2808 Console                   30    33 092 Ko
    tasklist.exe                 67772 Console                   30    10 056 Ko
    WmiPrvSE.exe                 41456 Services                   0     8 768 Ko




### Choisissez 5 services système et expliquer leur utilité

HidServ :	
    Permet l'accès entrant générique aux périphériques d'interface utilisateur, qui activent et maintiennent l'utilisation des boutons actifs prédéfinis sur le clavier, les contrôles à distance, et d'autres périphériques multimédia. Si ce service est arrêté, les boutons actifs contrôlés par ce service ne fonctionneront pas.

LmHosts : 	
    Permet la prise en charge pour NetBIOS sur un service TCP/IP (NetBT) et la résolution des noms NetBIOS.

Dhcp : 	
    Gère la configuration réseau en inscrivant et en mettant à jour les adresses IP et les noms DNS.

wuauserv :
Active le téléchargement et l'installation de mises à jour Windows critiques. Si le service est désactivé, le système d'exploitation peut être mis à jour manuellement sur le site Web de Windows Update.

wscsvc :
Analyse les paramètres de sécurité et les configurations du système.

### Déterminer les processus lancés par l'utilisateur qui est full admin sur la machine:
*PS C:\Windows\system32> Get-Process -includeusername |sort status, username*

    Handles      WS(K)   CPU(s)     Id UserName               ProcessName
    -------      -----   ------     -- --------               -----------
    [...]
        225       4200   246,61   1900 AUTORITE NT\SERVICE... svchost
       5471       7228   277,61   2556 AUTORITE NT\SERVICE... svchost
        471      14552    28,77   2504 AUTORITE NT\SERVICE... svchost
        172       2256     3,08  14360 AUTORITE NT\SERVICE... svchost
        235       2448     0,19   5852 AUTORITE NT\SERVICE... PresentationFontCache
        173       3156     7,30  12412 AUTORITE NT\SERVICE... svchost
        125       2692     0,56   1832 AUTORITE NT\SERVICE... svchost
        494      10272   263,30   3252 AUTORITE NT\SERVICE... svchost
        118       5552     0,14  64052 AUTORITE NT\SERVICE... svchost
        447      13340    29,89   6752 AUTORITE NT\SERVICE... svchost
        142       2736     8,72   3344 AUTORITE NT\SERVICE... svchost
        219       3908    17,14  17844 AUTORITE NT\SERVICE... svchost
        182       5244     4,89   2784 AUTORITE NT\SERVICE... svchost
        226       4296    37,31   3116 AUTORITE NT\SERVICE... svchost
        266       7668    25,69  46672 AUTORITE NT\SERVICE... svchost
        178       5412     9,56   7576 AUTORITE NT\SERVICE... svchost
        135       1364     0,25   4432 AUTORITE NT\SERVICE... svchost
        164       2412     1,55   1436 AUTORITE NT\SERVICE... svchost
        400       5184    16,27   4444 AUTORITE NT\SERVICE... svchost
        427      13196   173,56   3752 AUTORITE NT\SERVICE... svchost
        167       8396   126,02   1420 AUTORITE NT\SERVICE... svchost
        177       2472     8,08   1400 AUTORITE NT\SERVICE... svchost
        383      45008 1 227,69      8 AUTORITE NT\SERVICE... svchost
        242       3336     2,08   1448 AUTORITE NT\SERVICE... svchost
        475      13352    43,53   5780 AUTORITE NT\SERVICE... svchost
        397       5536    61,28   1612 AUTORITE NT\SERVICE... svchost
        158       5796     7,50   1796 AUTORITE NT\SERVICE... svchost
        220       3628    27,56   5772 AUTORITE NT\SERVICE... svchost
        220       3972     1,72   1472 AUTORITE NT\SERVICE... svchost
        176       4160     3,03   1572 AUTORITE NT\SERVICE... svchost
        136       1536     0,69   4836 AUTORITE NT\SERVICE... svchost
        252       2084     1,83   1304 AUTORITE NT\SERVICE... WUDFHost
        340       8504    17,66   2924 AUTORITE NT\SERVICE... dasHost
        212       2108     8,39   3132 AUTORITE NT\SERVICE... WUDFHost
        279       2744     0,81   2976 AUTORITE NT\SERVICE... WUDFHost
        422      15080     9,86  58240 AUTORITE NT\SERVICE... audiodg
        368      14580    14,27    972 AUTORITE NT\SERVICE... WUDFHost
        181       2596    12,00   3816 AUTORITE NT\SERVICE... svchost
        410      15200    49,91   3804 AUTORITE NT\SERVICE... svchost
       1535      18048   930,03   1084 AUTORITE NT\SERVICE... svchost
        286       5820 1 041,66   2768 AUTORITE NT\SERVICE... svchost
        394       9700   146,45   2988 AUTORITE NT\SERVICE... svchost
        256       2992     1,20  19272 AUTORITE NT\SERVICE... svchost
        134       2324     1,41   1104 AUTORITE NT\Système    svchost
        149       2940    51,11   3444 AUTORITE NT\Système    CxUIUSvc64
        202       3600     1,09   4016 AUTORITE NT\Système    CxUtilSvc
       1162       6520    81,73   1336 AUTORITE NT\Système    svchost
        707      44220   271,34   1200 AUTORITE NT\Système    svchost
        405       5324   102,42   1132 AUTORITE NT\Système    svchost
       1432      50048   894,20    556 AUTORITE NT\Système    svchost
      11507       5912   149,05   4408 AUTORITE NT\Système    svchost
        476      13992   588,83   4216 AUTORITE NT\Système    svchost
        276       8492     0,28  34632 AUTORITE NT\Système    winlogon
        265       7524     6,34   3488 AUTORITE NT\Système    svchost
        243      13468     8,80   4560 AUTORITE NT\Système    svchost
        263       3128    93,67   4132 AUTORITE NT\Système    svchost
        102       2144     3,61   3744 AUTORITE NT\Système    wlanext
         86       1172     0,22     88 AUTORITE NT\Système    svchost
        343       6344     8,47   3676 AUTORITE NT\Système    svchost
        148       1820     1,00   1148 AUTORITE NT\Système    SocketHeciServer
        635      12328    12,78   3616 AUTORITE NT\Système    spoolsv
        240       8620   909,00   2620 AUTORITE NT\Système    svchost
        271       1652   137,36   2632 AUTORITE NT\Système    svchost
        155       4580     0,00  64844 AUTORITE NT\Système    AppVShNotify
        794      17648   358,33   2432 AUTORITE NT\Système    svchost
        195       6516    54,11   2440 AUTORITE NT\Système    svchost
        211       4052    49,58   2776 AUTORITE NT\Système    svchost
       1848      14872   211,34   3388 AUTORITE NT\Système    svchost
        545      37364   150,42   3428 AUTORITE NT\Système    svchost
        177       2552     2,14   4080 AUTORITE NT\Système    ACCSvc
        439      13544    46,22   2792 AUTORITE NT\Système    svchost
        204       3312     0,34    768 AUTORITE NT\Système    aesm_service
        255       5980    39,16   1756 AUTORITE NT\Système    svchost
        225       6672    24,11   1784 AUTORITE NT\Système    svchost
        420      13032    59,38   1704 AUTORITE NT\Système    svchost
      17025      15876   488,27   3280 AUTORITE NT\Système    CxAudioSvc
        199       7852    25,33  14024 AUTORITE NT\Système    WmiPrvSE
        118       2172     0,06   3784 AUTORITE NT\Système    conhost
        231       5360     3,50   2236 AUTORITE NT\Système    svchost
        238       3892     1,69   2340 AUTORITE NT\Système    svchost
        750       3860     2,11   2164 AUTORITE NT\Système    svchost
        294       3472     0,25   3268 AUTORITE NT\Système    Bridge_Service
        279       5240     1,27   1988 AUTORITE NT\Système    svchost
        276       8840     0,27  65888 AUTORITE NT\Système    vds
        378       9956    54,64   3416 AUTORITE NT\Système    GTFidoService
        758      42100    17,16  67984 AUTORITE NT\Système    OfficeClickToRun
       3154      53704 ...45,58   4364 AUTORITE NT\Système    NortonSecurity
        192       3224     0,39  19356 AUTORITE NT\Système    svchost
        208       6104     1,81   2420 AUTORITE NT\Système    igfxCUIServiceN
        284      25448    85,28  11768 AUTORITE NT\Système    QAAdminAgent
        197       8284     0,11  67332 AUTORITE NT\Système    QALockHandler
        901      11416   283,05   8280 AUTORITE NT\Système    svchost
        689      10256     4,09   3288 AUTORITE NT\Système    OneApp.IGCC.WinService
        235       6708   136,02  11872 AUTORITE NT\Système    svchost
        517      20968     0,25  70140 AUTORITE NT\Système    svchost
        125       1848     0,08   4092 AUTORITE NT\Système    IntelCpHDCPSvc
        145       1232     0,14   5352 AUTORITE NT\Système    jhi_service
        265       7496     6,91   4600 AUTORITE NT\Système    svchost
       2139      19588 1 227,86    832 AUTORITE NT\Système    lsass
        168       6852     0,05  49856 AUTORITE NT\Système    svchost
        122       4168     0,08  47228 AUTORITE NT\Système    svchost
        297       8892     0,50  53696 AUTORITE NT\Système    svchost
        482      10944    32,98   4208 AUTORITE NT\Système    IntelAudioService
        199       7808     0,09  65988 AUTORITE NT\Système    igfxextN
        228       2788     0,19   4452 AUTORITE NT\Système    ThunderboltService
        176       7188     0,05  11856 AUTORITE NT\Système    unsecapp
        198       5512     1,97  10196 AUTORITE NT\Système    dllhost
        448       6428    10,22   5864 AUTORITE NT\Système    svchost
        129       2272     0,38   4656 AUTORITE NT\Système    svchost
        737      44372    15,08  44368 AUTORITE NT\Système    SearchIndexer
        176       7304     0,11  57108 AUTORITE NT\Système    unsecapp
        273       9080     2,14   5384 AUTORITE NT\Système    svchost
        520       8840   174,25   4884 AUTORITE NT\Système    svchost
        356      15528   248,13   6308 AUTORITE NT\Système    svchost
        148       1656     0,08   4424 AUTORITE NT\Système    RstMwService
        471      12008    76,27   7476 AUTORITE NT\Système    svchost
       1326       4472     2,88  13924 AUTORITE NT\Système    QASvc
        429      15768    47,53   4648 AUTORITE NT\Système    svchost
        117       2008     1,55   3800 AUTORITE NT\Système    esif_uf
        148       3932     2,47   6356 AUTORITE NT\Système    svchost
    [...]



## 5. Network

### Afficher la liste des cartes réseau de votre machine

*PS C:\Users\rouss> ipconfig*


    Configuration IP de Windows


    Carte Ethernet VirtualBox Host-Only Network :

       Suffixe DNS propre à la connexion. . . :
       Adresse IPv6 de liaison locale. . . . .: fe80::14bc:c8b7:a3d9:bc1c%14
       Adresse IPv4. . . . . . . . . . . . . .: 192.168.56.1
       Masque de sous-réseau. . . . . . . . . : 255.255.255.0
       Passerelle par défaut. . . . . . . . . :

    Carte réseau sans fil Connexion au réseau local* 1 :

       Statut du média. . . . . . . . . . . . : Média déconnecté
       Suffixe DNS propre à la connexion. . . :

    Carte réseau sans fil Connexion au réseau local* 2 :

       Statut du média. . . . . . . . . . . . : Média déconnecté
       Suffixe DNS propre à la connexion. . . :

    Carte réseau sans fil Wi-Fi :

       Suffixe DNS propre à la connexion. . . : lan
       Adresse IPv6 de liaison locale. . . . .: fe80::7dc0:aaa2:7e1c:1e1f%15
       Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.76
       Masque de sous-réseau. . . . . . . . . : 255.255.255.0
       Passerelle par défaut. . . . . . . . . : 192.168.1.254
       
 ### Expliquer la fonction de chacune d'entre elles

Carte Wifi (sans fil) :
            La carte wifi est une norme de communication permettant la transmission de données numériques sans fil. Elle est appelée carte réseau compatible avec la norme WI-FI et équipée d'une antenne émettrice / réceptrice . Elle est également appelée Network Interface Card ( NIC ). Elle permet de se connecter à un réseau via les ondes radios.

Carte Ethernet: 
	Elle permet de se connecter à un réseau en filaire. 

### Lister tous les ports TCP et UDP en utilisation et déterminer quel programme tourne derrière chacun des ports:
*PS C:\Windows\system32> netstat -a -b*

    Connexions actives

      Proto  Adresse locale         Adresse distante       État
      TCP    0.0.0.0:135            LAPTOP-9S2RQ9JE:0      LISTENING
      RpcSs
     [svchost.exe]
      TCP    0.0.0.0:445            LAPTOP-9S2RQ9JE:0      LISTENING
     Impossible d’obtenir les informations de propriétaire
      TCP    0.0.0.0:808            LAPTOP-9S2RQ9JE:0      LISTENING
     [OneApp.IGCC.WinService.exe]
      TCP    0.0.0.0:5040           LAPTOP-9S2RQ9JE:0      LISTENING
      CDPSvc
     [svchost.exe]
      TCP    0.0.0.0:49664          LAPTOP-9S2RQ9JE:0      LISTENING
     [lsass.exe]
      TCP    0.0.0.0:49665          LAPTOP-9S2RQ9JE:0      LISTENING
     Impossible d’obtenir les informations de propriétaire
      TCP    0.0.0.0:49666          LAPTOP-9S2RQ9JE:0      LISTENING
      Schedule
     [svchost.exe]
      TCP    0.0.0.0:49667          LAPTOP-9S2RQ9JE:0      LISTENING
      EventLog
     [svchost.exe]
      TCP    0.0.0.0:49668          LAPTOP-9S2RQ9JE:0      LISTENING
     [spoolsv.exe]
      TCP    0.0.0.0:49670          LAPTOP-9S2RQ9JE:0      LISTENING
     Impossible d’obtenir les informations de propriétaire
      TCP    10.33.18.222:139       LAPTOP-9S2RQ9JE:0      LISTENING
     Impossible d’obtenir les informations de propriétaire
      TCP    10.33.18.222:49541     40.67.254.36:https     ESTABLISHED
      WpnService
     [svchost.exe]
      TCP    10.33.18.222:59486     52.113.199.201:https   ESTABLISHED
     [Teams.exe]
      TCP    10.33.18.222:59489     40.67.254.36:https     ESTABLISHED
     [OneDrive.exe]
      TCP    10.33.18.222:59573     40.67.251.132:https    ESTABLISHED
     [msedge.exe]
      TCP    10.33.18.222:60463     52.114.76.152:https    ESTABLISHED
     [Teams.exe]
      TCP    10.33.18.222:60525     13.88.181.35:https     ESTABLISHED
     [NortonSecurity.exe]
      TCP    10.33.18.222:60769     162.159.129.233:https  ESTABLISHED
     [Discord.exe]
      TCP    10.33.18.222:60774     40.77.226.250:https    ESTABLISHED
     [msedge.exe]
      TCP    10.33.18.222:60781     bam-8:https            ESTABLISHED
     [msedge.exe]
      TCP    10.33.18.222:60783     162.159.134.234:https  ESTABLISHED
     [Discord.exe]
      TCP    10.33.18.222:60786     168.62.57.154:https    ESTABLISHED
     [FileCoAuth.exe]
      TCP    10.33.18.222:60787     168.62.57.154:https    ESTABLISHED
     [FileCoAuth.exe]
      TCP    10.33.18.222:60788     ionos:https            ESTABLISHED
     [msedge.exe]
      TCP    10.33.18.222:60792     t-bs:https             ESTABLISHED
     [msedge.exe]
      TCP    10.33.18.222:60793     pixel:https            CLOSE_WAIT
     [msedge.exe]
      TCP    10.33.18.222:60794     104.16.122.175:https   ESTABLISHED
     [msedge.exe]
      TCP    10.33.18.222:60795     pixel:https            CLOSE_WAIT
     [msedge.exe]
      TCP    10.33.18.222:60800     a-0001:https           ESTABLISHED
     [SearchUI.exe]
      TCP    10.33.18.222:60801     bingforbusiness:https  ESTABLISHED
     [SearchUI.exe]
      TCP    10.33.18.222:60802     52.114.132.73:https    ESTABLISHED
     [SearchUI.exe]
      TCP    10.33.18.222:60803     13.107.136.254:https   ESTABLISHED
     [SearchUI.exe]
      TCP    127.0.0.1:6463         LAPTOP-9S2RQ9JE:0      LISTENING
     [Discord.exe]
      TCP    127.0.0.1:49855        LAPTOP-9S2RQ9JE:0      LISTENING
     [SocketHeciServer.exe]
      TCP    127.0.0.1:55989        LAPTOP-9S2RQ9JE:0      LISTENING
     [Bridge_Service.exe]
      TCP    127.0.0.1:56989        LAPTOP-9S2RQ9JE:0      LISTENING
     [Bridge_Service.exe]
      TCP    [::]:135               LAPTOP-9S2RQ9JE:0      LISTENING
      RpcSs
     [svchost.exe]
      TCP    [::]:445               LAPTOP-9S2RQ9JE:0      LISTENING
     Impossible d’obtenir les informations de propriétaire
      TCP    [::]:808               LAPTOP-9S2RQ9JE:0      LISTENING
     [OneApp.IGCC.WinService.exe]
      TCP    [::]:49664             LAPTOP-9S2RQ9JE:0      LISTENING
     [lsass.exe]
      TCP    [::]:49665             LAPTOP-9S2RQ9JE:0      LISTENING
     Impossible d’obtenir les informations de propriétaire
      TCP    [::]:49666             LAPTOP-9S2RQ9JE:0      LISTENING
      Schedule
     [svchost.exe]
      TCP    [::]:49667             LAPTOP-9S2RQ9JE:0      LISTENING
      EventLog
     [svchost.exe]
      TCP    [::]:49668             LAPTOP-9S2RQ9JE:0      LISTENING
     [spoolsv.exe]
      TCP    [::]:49670             LAPTOP-9S2RQ9JE:0      LISTENING
     Impossible d’obtenir les informations de propriétaire
      TCP    [::1]:49669            LAPTOP-9S2RQ9JE:0      LISTENING
     [jhi_service.exe]
      UDP    0.0.0.0:500            *:*
      IKEEXT
     [svchost.exe]
      UDP    0.0.0.0:3702           *:*
     [dashost.exe]
      UDP    0.0.0.0:3702           *:*
     [dashost.exe]
      UDP    0.0.0.0:4500           *:*
      IKEEXT
     [svchost.exe]
      UDP    0.0.0.0:5050           *:*
      CDPSvc
     [svchost.exe]
      UDP    0.0.0.0:5353           *:*
     [msedge.exe]
      UDP    0.0.0.0:5353           *:*
      Dnscache
     [svchost.exe]
      UDP    0.0.0.0:5353           *:*
     [msedge.exe]
      UDP    0.0.0.0:5353           *:*
     [msedge.exe]
      UDP    0.0.0.0:5353           *:*
     [msedge.exe]
      UDP    0.0.0.0:5355           *:*
      Dnscache
     [svchost.exe]
      UDP    0.0.0.0:54493          *:*
     [dashost.exe]
      UDP    0.0.0.0:57160          *:*
     [Teams.exe]
      UDP    0.0.0.0:62148          *:*
     [Teams.exe]
      UDP    10.33.18.222:137       *:*
     Impossible d’obtenir les informations de propriétaire
      UDP    10.33.18.222:138       *:*
     Impossible d’obtenir les informations de propriétaire
      UDP    10.33.18.222:1900      *:*
      SSDPSRV
     [svchost.exe]
      UDP    10.33.18.222:2177      *:*
      QWAVE
     [svchost.exe]
      UDP    10.33.18.222:57018     *:*
      SSDPSRV
     [svchost.exe]
      UDP    127.0.0.1:1900         *:*
      SSDPSRV
     [svchost.exe]
      UDP    127.0.0.1:49664        *:*
      iphlpsvc
     [svchost.exe]
      UDP    127.0.0.1:57019        *:*
      SSDPSRV
     [svchost.exe]
      UDP    127.0.0.1:63064        *:*
      NlaSvc
     [svchost.exe]
      UDP    [::]:500               *:*
      IKEEXT
     [svchost.exe]
      UDP    [::]:3702              *:*
     [dashost.exe]
      UDP    [::]:3702              *:*
     [dashost.exe]
      UDP    [::]:4500              *:*
      IKEEXT
     [svchost.exe]
      UDP    [::]:5353              *:*
      Dnscache
     [svchost.exe]
      UDP    [::]:5353              *:*
     [msedge.exe]
      UDP    [::]:5353              *:*
     [msedge.exe]
      UDP    [::]:5355              *:*
      Dnscache
     [svchost.exe]
      UDP    [::]:54494             *:*
     [dashost.exe]
      UDP    [::]:57160             *:*
     [Teams.exe]
      UDP    [::]:62148             *:*
     [Teams.exe]
      UDP    [::1]:1900             *:*
      SSDPSRV
     [svchost.exe]
      UDP    [::1]:57017            *:*
      SSDPSRV
     [svchost.exe]
      UDP    [fe80::7dc0:aaa2:7e1c:1e1f%14]:1900  *:*
      SSDPSRV
     [svchost.exe]
      UDP    [fe80::7dc0:aaa2:7e1c:1e1f%14]:2177  *:*
      QWAVE
     [svchost.exe]
      UDP    [fe80::7dc0:aaa2:7e1c:1e1f%14]:57016  *:*
      SSDPSRV
     [svchost.exe]

### Expliquer la fonction de chacun de ces programmes

[svchost.exe] :	
    Au démarrage, svchost.exe vérifie le registre pour des services chargeant un fichier . dll externe et les démarrent. C'est normal qu'il y ait plus d'un processus en fonction à la fois; chacun d'eux représente un groupe de services essentiels s'exécutant sur le PC.

[Teams.exe] :	 
     Application

[msedge.exe] :	 
     Composant système de Windows. Le fichier est un fichier signé Microsoft. Certifié par un organisme de confiance. MicrosoftEdge.exe est capable de contrôler d'autres programmes

[dashost.exe]:	 
     Ce programme est chargé au démarrage de Windows (voir la clé de Registre : Run ). Le programme utilise les ports pour se connecter au LAN ou Internet. Ce n'est pas un composant système de Windows. dasHost.exe s'apparente à un fichier compressé.
 
[lsass.exe] :	 
     lsass.exe (Local Security Authority Subsystem) est un exécutable qui est nécessaire pour le bon fonctionnement de Windows. Il assure l'identification des utilisateurs (utilisateurs du domaine ou utilisateurs locaux).

[jhi_service.exe] :	 
     Le service JHI ("jhi_service.exe") est appelé un composant du moteur de gestion Intel®, bien qu'il ne se trouve pas dans le moteur de gestion réel (ME).

[spoolsv.exe] :	
    spoolsv.exe s'occupe de l'impression, mais est aussi connu comme le troyen Backdoor.Ciadoor.B Trojan.

[FileCoAuth.exe] :	 
     FileCoAuth.exe est capable de surveiller les applications et d'enregistrer les entrées du clavier et de la souris

[OneDrive.exe] :	 
     Application

[OneApp.IGCC.WinService.exe]:

[SocketHeciServer.exe] : 	
    Ce programme a une fenêtre invisible. Ce n'est pas un composant système de Windows. Signé numériquement. L'application écoute ou envoie des données sur les ports ouverts du LAN ou sur Internet.

[NortonSecurity.exe] :	 
    Application

[Discord.exe] : 	
    Application

[SearchUI.exe]:	
    Le fichier SearchUI.exe est celui qui gère la fonction de recherche de Cortana. ... Ce type de problème peut être dû à des fichiers système manquants ou à des problèmes avec le programme Cortana lui-même.

[Bridge_Service.exe] :	
    Le programme n'est pas visible. Ce n'est pas un fichier de base Windows. BridgeService.exe est capable de se connecter à Internet, d'enregistrer les entrées du clavier et de la souris et de surveiller les applications. Par conséquent, la cote de sécurité technique est dangereuse à 48% .

## III. Gestion de softs


Un gestionnaire de paquets permet d’installer, de désinstaller et de mettre à jour des logiciels.

Les avantages de ce genre d'outils sont nombreux:


 - Tous les logiciels (les paquets) sont centralisés dans un seul et même serveur (le dépôt). Il n'est plus nécessaire de parcourir le web pour trouver les fichiers d'installations. Tout peux être installé et mis à jour en une seule commande.
 - Tous les logiciels sont dépourvus de spywares et malwares en tout genre.
 - Les dépendances sont automatiquement installées : si un logiciel a besoin d’un programme pour fonctionner, le gestionnaire va l'installer automatiquement.
 - Elle respecte votre vie privée car elle ne demande pas d’enregistrement qui permettrait de stocker des informations permettant de suivre vos usages.

### Liste des paquets installés

*PS C:\Windows\system32> choco list -l*

    Chocolatey v0.10.15
    chocolatey 0.10.15
    1 packages installed.

    Did you know Pro / Business automatically syncs with Programs and
     Features? Learn more about Package Synchronizer at
     https://chocolatey.org/compare

### Provenance des paquets

*PS C:\Windows\system32> choco source*

    Chocolatey v0.10.15
    chocolatey - https://chocolatey.org/api/v2/ | Priority 0|Bypass Proxy - False|Self-Service - False|Admin Only - False.

## IV. Partage de fichiers

#### 1-Création de dossier partagé sur mon ordinateur(sous windows):

*PS C:\Windows\system32> New-SmbShare -Name shared -Path C:\Users\ttgct\test -FullAccess "ttgct"*

    Name   ScopeName Path                Description
    ----   --------- ----                -----------
    shared *         C:\Users\ttgct\test


##### Je crée un nouveau dossier "filetest.txt" dans le dossier partager:

*PS C:\Users\ttgct\test> New-Item -Path 'C:\Users\ttgct\test\filetest.txt' -ItemType File*


    Répertoire : C:\Users\ttgct\test


    Mode                LastWriteTime         Length Name
    ----                -------------         ------ ----
    -a----       10/11/2020     11:14              0 filetest.txt



#### 2- Accéder au fichier depuis ma VM:

##### J'installe d'abord le paquet cifs-utils:

    [david@localhost ~]$ sudo yum install -y cifs-utils

##### Je crée ensuite un dossier de partage: 

    [david@localhost ~]$ mkdir /opt/partage

##### Je crée ensuite un montage réseau pour accéder au dossier partagé créer sur mon ordinateur:

    [david@localhost ~]$ sudo mount -t cifs -o username=ttgct,password=Richardolivier! //192.168.120.1/shared /opt/partage

##### Je vérifie que j'ai bien accès au partage:

*[david@localhost ~]$ cd /opt/partage*
*[david@localhost partage]$ ls*

    filetest.txt

##### Je vérifie les droits que j'ai sur ce fichier:

*[david@localhost partage]$ ls -l filetest.txt*

    -rwxr-xr-x. 1root root 0 Nov 10 11:14 filetest.txt

##### Je supprime le fichier "filetest.txt" et j'en créer un nouveau "test.txt":

    [david@localhost partage]$ sudo rm filetest.txt
    [david@localhost partage]$ sudo touch test.txt

##### Je vérifie qu'il a bien été crée: 

*[david@localhost partage]$ ls*

    test.txt
 
#### 3-Je vérifie depuis mon ordinateur que le fichier "test" a bien été créé:

*PS C:\Users\ttgct\test> ls*


    Répertoire : C:\Users\ttgct\test


    Mode                LastWriteTime         Length Name
    ----                -------------         ------ ----
    -a----       10/11/2020     11:46              0 test.txt