<#David Roussat#>
<#10.11.2020#>
<#script to lock or shutdown the laptop with arguments#>

param(
           
    [Parameter(Mandatory = $true)] 
    [string]$arg,
    [Parameter(Mandatory = $true)]
    [int]$time
)
$time = (Start-Sleep -s($time))
 
<#if we want to lock#>
<#enter in the first argument "lock"#>
<#enter in the second argument, a number (in second) to start counting down to lock the laptop #>
if ($arg -eq "lock") { 
    $LOCK = (rundll32.exe user32.dll, LockWorkStation) 
    $LOCK 
     
} 
<#if we want to shutdown#>
<#enter in the first argument "shutdown"#>
<#enter in the second argument, a number (in second) to start counting down to shutdown the laptop #>
if ($arg -eq "shutdown") {
    $Shut = (Stop-Computer -Force)
    $Shut 
    
}
    


   
